 
# Areas_in_a_Board

[Areas_in_a_Board](https://gitlab.com/bamathis/Areas_in_a_Board) is a recursive program that determines the number of white areas in an 8x8 board from an input text file.

## Quick Start

### Program Execution

```
$ ./Main.cpp filename
```

### File Input

Reads a given input file, where each line represents a row of the board and contains a w or b representing the color of the block.