						//Name: Brandon Mathis
						//File Name: Main.cpp
						//Date: 19 September 2014
						//Program Description: Areas in board counter

#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;

const int COLUMN = 8;
const int ROW = 8;
void loadBoard(char origArray[][COLUMN],char fileName[]);
void displayBoard (char origArray[][COLUMN]);
void processArea(char origArray[][COLUMN], bool checkedArray[][COLUMN],int & numCells, int column, int row);

int main (int argc, char* argv[])
{
int numCells;
int numAreas;
char origArray [ROW + 1][COLUMN];
bool checkedArray [ROW][COLUMN] = {0};
numAreas = 0;
numCells = 0;
loadBoard(origArray,argv[1]);
displayBoard(origArray);
cout << endl;
cout << "Number of cells in areas: ";
for(int i = 0; i < ROW; i++)	//loops every row of origArray that contains letters
    {
    for(int j = 0; j < COLUMN; j++)	//loops every column of origArray
        {
        if (!checkedArray[i][j] && origArray[i][j] == 'w')	//Checks if the current block has been visited and is white
            {
            numCells = 0;
            processArea(origArray,checkedArray,numCells,j,i);
            numAreas++;	//adds one to numAreas if it is a new section of white blocks
            cout << numCells << " ";
            }
        }
    }
cout << endl;
cout << "Number of areas: " << numAreas << endl;
return 0;
}
//==========================================================
//Puts the contents of a specified file into origArray
void loadBoard(char origArray[][COLUMN], char fileName[])
{
ifstream inFile;
inFile.open(fileName);
if (inFile.fail())	//checks if the file is present and can be opened
    exit(1);
for(int i = 0; i < ROW; i++)	//loops every row of origArray that contains letters
    {
    for (int j = 0; j < COLUMN; j++)	//loops every column of origArray
        {
        origArray[i][j] = inFile.get();	//adds characters one at a time in to the array
        }
    inFile.ignore(999,'\n');
    }
origArray [ROW][0] = '\0';	//adds a terminator to the end of the array
inFile.close();
}
//=============================================================
//Displays all the contents of origArray
void displayBoard (char origArray[][ROW])
{
for(int i = 0; i < ROW; i++)	//loops every row of origArray that contains letters
    {
    for (int j = 0; j < COLUMN; j++)	//loops every column of origArray
        {
        cout << origArray[i][j];
        }
    cout << endl;
    }
}
//==============================================================
//Recursively searches up, down, left, and right of the passed array position for all connected white blocks
void processArea(char origArray[][COLUMN], bool checkedArray[][COLUMN],int & numCells,int column, int row)
{
checkedArray[row][column] = true;		//sets a 1 for the current array position
if(checkedArray [row][column] != 0 && origArray[row][column] == 'w') //adds 1 to numCells if the position hasn't been checked and is white
    numCells++;
if (column >= 0 && column + 1 < COLUMN && !checkedArray[row][column + 1] && origArray[row][column + 1]!= 'b') //searches right from current position
    processArea (origArray, checkedArray,numCells,column + 1,row);
if (row >= 0 &&  row + 1 < ROW && !checkedArray[row + 1 ][column] && origArray[row + 1][column]!= 'b')  //searches down from current position
    processArea(origArray, checkedArray,numCells, column, row + 1);
if (column > 0 && !checkedArray[row][column - 1] && origArray[row][column - 1]!= 'b') //searches left from current position
    processArea(origArray, checkedArray,numCells, column - 1 ,row);
if(row > 0  && !checkedArray[row - 1][column] && origArray[row - 1][column]!= 'b')  //searches up from current position
    processArea(origArray, checkedArray,numCells, column, row - 1);
}

